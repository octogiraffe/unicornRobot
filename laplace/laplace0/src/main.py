vexcode_brain_precision = 0
vexcode_console_precision = 0
vexcode_controller_1_precision = 0
myVariable = 0

def when_started1():
    global myVariable, vexcode_brain_precision, vexcode_console_precision, vexcode_controller_1_precision
    brain.screen.clear_screen()
    controller_1.screen.clear_screen()
    print("Started", end="")
    controller_1.screen.print("DND")
    brain.screen.print("Take Me On Chumps!")

def onauton_autonomous_0():
    """
    Executes the autonomous routine for the robot.
    It Drives the drivetrain forward and waits for 15 seconds.
    """
    global myVariable, vexcode_brain_precision, vexcode_console_precision, vexcode_controller_1_precision
    brain.screen.clear_screen()
    controller_1.screen.clear_screen()
    print("Autonomous", end="")
    controller_1.screen.print("DND")
    brain.screen.print("Take, Me Out To The Ball Game")
    drivetrain.drive(FORWARD)
    wait(15, SECONDS)

def ondriver_drivercontrol_0():
    global myVariable, vexcode_brain_precision, vexcode_console_precision, vexcode_controller_1_precision
    brain.screen.clear_screen()
    controller_1.screen.clear_screen()
    print("Driver Control", end="")
    controller_1.screen.print("Here We GO!")
    brain.screen.print("We Live")

# create a function for handling the starting and stopping of all autonomous tasks
def vexcode_auton_function():
    # Start the autonomous control tasks
    auton_task_0 = Thread( onauton_autonomous_0 )
    # wait for the driver control period to end
    while( competition.is_autonomous() and competition.is_enabled() ):
        # wait 10 milliseconds before checking again
        wait( 10, MSEC )
    # Stop the autonomous control tasks
    auton_task_0.stop()

def vexcode_driver_function():
    # Start the driver control tasks
    driver_control_task_0 = Thread( ondriver_drivercontrol_0 )

    # wait for the driver control period to end
    while( competition.is_driver_control() and competition.is_enabled() ):
        # wait 10 milliseconds before checking again
        wait( 10, MSEC )
    # Stop the driver control tasks
    driver_control_task_0.stop()


# register the competition functions
competition = Competition( vexcode_driver_function, vexcode_auton_function )

when_started1()
